﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using U_Coder.Managers;

namespace U_Coder
{
    public partial class MainWindow : Form
    {
        private const int MAXLINES = 2000;
        private string titleName;
        private int topLine, caretLine, lastLineCount = 0;

        private FileManager fileManager;
        private LanguageModeManager lmm;

        #region Main Constructor
        public MainWindow()
        {
            InitializeComponent();
            titleName = this.Text;
            fileManager = new FileManager();
            lmm = new LanguageModeManager();

            for (int i = 1; i < MAXLINES; i++)
                richLineNumbers.AppendText(i + "\n");

            setTitle(lmm.languageModeNames[lmm.languageMode], titleName, fileManager.openedFile, "");

        }
        #endregion

        #region Text Changed
        private void richCodeField_TextChanged(object sender, EventArgs e)
        {

            if (!fileManager.Saved)
                setTitle(lmm.languageModeNames[lmm.languageMode], titleName, fileManager.openedFile, "*");
            fileManager.Saved = false;

            // Delete all
            if (richCodeField.Text == "")
                richCodeField.VScrollPosition = 0;

        }
        #endregion

        #region Keyboard

        private void richCodeField_KeyDown(object sender, KeyEventArgs e)
        {         
            if (e.Control && e.KeyCode == Keys.V)
            {
                richCodeField.Text += (string)Clipboard.GetData("Text");
                e.Handled = true;
            }

            if ( // Disable bleeping...
                richCodeField.GetLineFromCharIndex(richCodeField.SelectionStart) == 0 && e.KeyData == Keys.Up ||
                richCodeField.GetLineFromCharIndex(richCodeField.SelectionStart) == richCodeField.GetLineFromCharIndex(richCodeField.TextLength) && e.KeyData == Keys.Down ||
                richCodeField.SelectionStart == richCodeField.TextLength && e.KeyData == Keys.Right ||
                richCodeField.SelectionStart == 0 && e.KeyData == Keys.Left
                ) e.Handled = true;
        }

        #endregion

        #region ScrollbarFix

        private void richCodeField_vScroll(Message msg)
        {
            msg.HWnd = richLineNumbers.Handle;
            richLineNumbers.PubWndProc(ref msg);
        }

        #endregion

        #region Checkers

        private void exitToolStripMenuItem_Click(object sender, EventArgs e) // Exit
        {
            Application.Exit();
        }

        private void toolSettingsPascal_Click(object sender, EventArgs e)
        {
            toolSettingsPascal.CheckState = CheckState.Checked;
            toolSettingsCplus.CheckState = CheckState.Unchecked;
            lmm.languageMode = 1;
            setTitle(lmm.languageModeNames[lmm.languageMode], titleName, fileManager.openedFile, "");
        }

        private void toolSettingsCplus_Click(object sender, EventArgs e)
        {
            toolSettingsCplus.CheckState = CheckState.Checked;
            toolSettingsPascal.CheckState = CheckState.Unchecked;
            lmm.languageMode = 0;
            setTitle(lmm.languageModeNames[lmm.languageMode], titleName, fileManager.openedFile, "");
        }

        #endregion

        //------------------------------FILE MANAGER-----------------------------

        #region File
        private void newToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                toolBottomFilepath.Text = saveFileDialog1.FileName;
                fileManager.MakeNewFile(saveFileDialog1, ref richCodeField);
                setTitle(lmm.languageModeNames[lmm.languageMode], titleName, fileManager.openedFile, "");
                if (!richCodeField.Enabled) richCodeField.Enabled = true;
            }
        }

        private void openToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                toolBottomFilepath.Text = openFileDialog1.FileName;
                fileManager.GetFileContent(openFileDialog1, ref richCodeField);
                setTitle(lmm.languageModeNames[lmm.languageMode], titleName, fileManager.openedFile, "");
                if (!richCodeField.Enabled) richCodeField.Enabled = true;
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!richCodeField.Enabled) return;
            fileManager.SaveFile(richCodeField);
            setTitle(lmm.languageModeNames[lmm.languageMode], titleName, fileManager.openedFile, "");
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!richCodeField.Enabled) return;
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                fileManager.SaveFileAs(saveFileDialog1, richCodeField);
                setTitle(lmm.languageModeNames[lmm.languageMode], titleName, fileManager.openedFile, "");
            }
        }

        #endregion

        //------------------------------ Functions -----------------------------

        #region Functions
        
        private void setTitle(string lm, string tit, string fn, string s)
        {
            this.Text = lm + " " + tit + " | " + s + fn;
        }

        #endregion

        #region UndoRedo
        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richCodeField.Undo();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richCodeField.Redo();
        }
        #endregion

        private void richCodeField_SelectionChanged(object sender, EventArgs e)
        {
            int index = richCodeField.SelectionStart;
            caretLine = richCodeField.GetLineFromCharIndex(index);
            int caretColumn = richCodeField.SelectionStart - richCodeField.GetFirstCharIndexFromLine(caretLine);
            toolBottomSelected.Text = caretLine + 1 + ":" + caretColumn + 1;
        }

    }
}

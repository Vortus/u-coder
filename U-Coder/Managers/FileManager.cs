﻿using System;
using System.IO;
using System.Windows.Forms;

namespace U_Coder
{
    class FileManager
    {
        public string openedFile, openedFilePath;
        public bool Saved;

        public FileManager()
        {
            openedFile = "";
            openedFilePath = "";
            Saved = true;
        }

        public void MakeNewFile(SaveFileDialog sfd, ref CustomRichTextBox crtb)
        {
            if (sfd.FileName != "")
            {
                FileStream fs = (FileStream)sfd.OpenFile();
                crtb.Text = "//------------------------------ U - Coder -------------------------------";
                crtb.SaveFile(fs, RichTextBoxStreamType.PlainText);
                fs.Close();
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.FileName = sfd.FileName;
                GetFileContent(ofd, ref crtb);
            }
        }

        public void SaveFileAs(SaveFileDialog sfd, CustomRichTextBox crtb)
        {
            if (sfd.FileName != "")
            {
                FileStream fs = (FileStream)sfd.OpenFile();
                crtb.SaveFile(fs, RichTextBoxStreamType.PlainText);
                fs.Close();
            }
            sfd.FileName = ""; 
        }

        public void SaveFile(CustomRichTextBox crtb)
        {
            if (openedFile.Length > 0)
                crtb.SaveFile(openedFilePath, RichTextBoxStreamType.PlainText);
        }

        public void GetFileContent(OpenFileDialog ofd, ref CustomRichTextBox crtb)
        {
            StreamReader sr = new StreamReader(ofd.FileName);
            Saved = true;
            openedFile = ofd.SafeFileName;
            openedFilePath = ofd.FileName;
            crtb.Text = sr.ReadToEnd();
            sr.Close();
            ofd.FileName = ""; 
        }

    }
}

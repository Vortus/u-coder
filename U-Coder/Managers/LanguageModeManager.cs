﻿using System;

namespace U_Coder.Managers
{
    class LanguageModeManager
    {
        public int languageMode;
        public string[] languageModeNames;

        public LanguageModeManager()
        {
            languageMode = 0;
            languageModeNames = new string[2];
            languageModeNames[0] = "C++";
            languageModeNames[1] = "Pascal";
        }

    }
}

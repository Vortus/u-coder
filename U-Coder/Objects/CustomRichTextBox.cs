﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace U_Coder
{
    class CustomRichTextBox : RichTextBox
    {
        public event vScrollEventHandler vScroll;
        public delegate void vScrollEventHandler(Message msg);

        private const int SB_HORZ = 0x0000;
        private const int SB_VERT = 0x0001;
        private const int WM_HSCROLL = 0x0114;
        private const int WM_VSCROLL = 0x115;
        private const int WM_KEYDOWN = 0x0100;
        private const int SB_THUMBPOSITION = 4;

        private enum ScrollInfoMask : uint
        {
            SIF_RANGE = 0x1,
            SIF_PAGE = 0x2,
            SIF_POS = 0x4,
            SIF_DISABLENOSCROLL = 0x8,
            SIF_TRACKPOS = 0x10,
            SIF_ALL = (SIF_RANGE | SIF_PAGE | SIF_POS | SIF_TRACKPOS)
        }

        [StructLayout(LayoutKind.Sequential)]
        struct SCROLLINFO
        {
            public uint cbSize;
            public uint fMask;
            public uint nMin;
            public uint nMax;
            public uint nPage;
            public uint nPos;
            public uint nTrackPos;
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            switch (m.Msg)
            {
                case WM_VSCROLL:

                    if (vScroll != null) vScroll(m);
                    break;

                case WM_KEYDOWN: // Updating if key pressed

                    switch (m.WParam.ToInt32())
                    {
                        case (int)Keys.Down:
                            VScrollPosition += 0;
                            break;
                        case (int)Keys.Up:
                            VScrollPosition += 0;
                            break;
                        case (int)Keys.Enter:
                            VScrollPosition += 0;
                            break;
                    }

                    break;
            }

        }

        public void PubWndProc(ref Message msg)
        {
            base.WndProc(ref msg);
        }

        public int VScrollPosition
        {
            get
            {
                return GetScrollPos(this.Handle, SB_VERT);
            }
            set
            {
                SetScrollPos((IntPtr)Handle, SB_VERT, value, true);
                PostMessageA((IntPtr)Handle, WM_VSCROLL, SB_THUMBPOSITION + 0x10000 * value, 0);
            }
        }

        public int VScrollPositionMax
        {
            get
            {
                SCROLLINFO scrollInfo = new SCROLLINFO();
                scrollInfo.fMask = (uint)ScrollInfoMask.SIF_ALL;
                scrollInfo.cbSize = (uint)Marshal.SizeOf(scrollInfo);
                GetScrollInfo(Handle, SB_VERT, ref scrollInfo);
                return (int)scrollInfo.nMax - (int)scrollInfo.nPage;
            }
        }

        //--------------------------------- DLL -------------------------------------

        [DllImport("user32.dll")]
        static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int GetScrollPos(IntPtr hWnd, int nBar);

        [DllImport("user32.dll")]
        private static extern bool PostMessageA(IntPtr hWnd, int nBar, int wParam, int lParam);

        [DllImport("user32")]
        private static extern int GetScrollInfo(IntPtr hwnd, int nBar, ref SCROLLINFO scrollInfo);

    }
}
